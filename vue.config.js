module.exports = {
  pluginOptions: {
    electronBuilder: {
      preload: 'src/preload.js',
      files: [
        {
          "from": "src/assets/audio.png",
          "to": "/"
        }
      ]
    }
  },
  chainWebpack: config => {
    config
      .plugin('html')
      .tap(args => {
        args[0].title = 'AudioLibaray'
        return args
      })

    const svgRule = config.module.rule('svg');
    svgRule.uses.clear();
    svgRule
      .use('babel-loader')
      .loader('babel-loader')
      .end()
      .use('vue-svg-loader')
      .loader('vue-svg-loader');
  },
  transpileDependencies: [
    'vuetify'
  ],
  
}
