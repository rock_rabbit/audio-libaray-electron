const { contextBridge, ipcRenderer } = require('electron')
contextBridge.exposeInMainWorld('electron', {
  startDrag: (fileName) => {
    ipcRenderer.send('ondragstart', fileName)
  },
  setAlwaysOnTop: (b) => {
    ipcRenderer.send('onSetAlwaysOnTop', b)
  },
  openExternal: (u) =>{
    ipcRenderer.send('onOpenExternal', u)
  }
})