const { app, Menu } = require('electron')

const isMac = process.platform === 'darwin'

const template = [
  // { role: 'appMenu' }
  ...(isMac ? [{
    label: app.name,
    submenu: [
      { role: 'about',label:"关于" },
      { type: 'separator' },
      { role: 'services',label:"服务" },
      { type: 'separator' },
      { role: 'hide',label:"隐藏"+app.name },
      { role: 'hideothers',label:"隐藏其他" },
      { role: 'unhide',label:"全部显示"  },
      { type: 'separator' },
      { role: 'quit',label:"退出"+app.name}
    ]
  }] : [])
]

const menu = Menu.buildFromTemplate(template)
Menu.setApplicationMenu(menu)