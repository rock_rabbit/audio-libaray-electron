import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import FixFull from '../assets/fix-full.svg'

Vue.use(Vuetify);

export default new Vuetify({
    icons: {
        // iconfont: 'mdiSvg',
        values: {
            fixfull: FixFull,
        },
    },
});
