import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import cfg from './config/config'
import WaveSurfer from "wavesurfer.js";
import axios from "./requests/axios";

// WaveSurfer 是一个频谱显示和音频播放的插件
Vue.prototype.$WaveSurfer = WaveSurfer
// axios 是主要的 http 请求方式
Vue.prototype.$http = axios
// config 是一些全局配置
Vue.prototype.$config = cfg
Vue.config.productionTip = false
// vuetify 是一个 vue 的 ui 库
new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
